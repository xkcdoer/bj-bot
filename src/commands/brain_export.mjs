import { SlashCommandBuilder, PermissionFlagsBits, AttachmentBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('brain_export')
  .setDescription('Export the bot\'s brain data.')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  try {
    await interaction.deferReply({ ephemeral: true });
    await interaction.editReply({ content: 'Please hold on. I am working on exporting the brain data...'});
    const fileAttachment = new AttachmentBuilder(process.env.BRAIN_FILE);
    await interaction.editReply({
      content: 'Here is the bot\'s brain data export file:',
      files: [fileAttachment],
      ephemeral: true
    });
  } catch (error) {
    console.error(error);
    await interaction.editReply({
      content: 'An error occurred while exporting the bot\'s brain data. Please check the bot logs for details.',
      ephemeral: true
    });
  }
}
