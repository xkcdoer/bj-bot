import { SlashCommandBuilder } from 'discord.js';
import { revokeConsent, revokeConsentFromAll } from '../../utils/consent.mjs';
import {getGuildMemberByUserID} from '../../utils/users.mjs';

export const data = new SlashCommandBuilder()
  .setName('consent_revoke')
  .setDescription('Revoke consent from a user or from all users')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user you want to revoke consent from')
      .setRequired(false)
  );

export async function execute(interaction) {
  const user = interaction.options.getUser('user');

  if (user) {
    let revokeStatus = await revokeConsent(interaction.user, user);
    if (revokeStatus) {
      const member = await getGuildMemberByUserID(interaction.guild, user.id);
      await interaction.reply(`You've revoked consent from: ${member.displayName}`);
    } else {
      await interaction.reply('I wasn\'t able to find any consent on record to that user. ' +
        'If this is a mistake, please let beta know.\n' +
        'Please note that you cannot revoke consent to someone you did not explicitly ' +
        'give consent to ("all" doesn\'t count).');
    }
  } else {
    let revokeAllStatus = await revokeConsentFromAll(interaction.user);
    if (revokeAllStatus) {
      await interaction.reply('Consent to everyone has been revoked.');
    } else {
      await interaction.reply('I couldn\'t find consent to everyone, so I haven\'t ' +
        'changed anything. If this is a mistake, please let beta know.');
    }
  }
}
