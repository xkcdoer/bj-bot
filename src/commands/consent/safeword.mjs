import { SlashCommandBuilder } from 'discord.js';
import { useSafeword } from '../../utils/consent.mjs';

export const data = new SlashCommandBuilder()
  .setName('safeword')
  .setDescription('Activate your safeword and pause all consent to the server');

export async function execute(interaction) {
  if (!await useSafeword(interaction.user)) {
    await interaction.reply('Your safeword was already on.');
  } else {
    await interaction.reply('Okay. No one can mess with you right now. Take a break. ' +
      'When you want to end your safeword, use /end_safeword');
  }
}
