import {SlashCommandBuilder} from 'discord.js';
import {brain, ensureBrainKeyExists} from '../../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('counting_shapes_give_up')
  .setDescription('Give up and have me tell you how many shapes were there.');

export async function execute(interaction) {
  const user = interaction.user;

  let shape_record = ensureBrainKeyExists('shape_count');
  if (!shape_record[user.id]) {
    return interaction.reply('You aren\'t counting any shapes right now. ' +
      'Use `/counting_shapes_start` to get started!'
    );
  }

  const correctCount = shape_record[user.id].count;
  const messageLink = shape_record[user.id].message_link;

  // Delete the user's record as they gave up
  delete shape_record[user.id];

  await brain.write(); // save changes to the brain

  return interaction.reply(`You gave up! There were ${correctCount} shapes. Here's the [original image](${messageLink}).`);
}
