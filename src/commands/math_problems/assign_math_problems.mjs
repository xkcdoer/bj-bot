import { SlashCommandBuilder } from 'discord.js';
import { problemUtils } from '../../utils/math_problems.mjs';
import { hasConsent } from '../../utils/consent.mjs';

export const data = new SlashCommandBuilder()
  .setName('assign_math')
  .setDescription('Assigns math problems to a user or gives times table problems')

  .addSubcommand(subcommand =>
    subcommand
      .setName('problems')
      .setDescription('Assigns general math problems')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to assign a problem to.')
        .setRequired(true))
      .addIntegerOption(option => option.setName('num_problems')
        .setDescription('The number of problems the user should solve.')
        .setRequired(true))
      .addStringOption(option => option.setName('operation')
        .setDescription('Limit to one operation for the math problems.')
        .setRequired(false)
        .addChoices(
          { name: 'Addition', value: 'addition' },
          { name: 'Subtraction', value: 'subtraction' },
          { name: 'Multiplication', value: 'multiplication' },
          { name: 'Division', value: 'division' },
          { name: 'All', value: 'all' }
        ))
      .addBooleanOption(option => option.setName('hard_mode')
        .setDescription('Enable hard mode.')
        .setRequired(false))
      .addBooleanOption(option => option.setName('math_test')
        .setDescription('Take a math test! Wrong answers aren\'t immediately revealed.')
        .setRequired(false))
      .addStringOption(option => option.setName('spectator')
        .setDescription('Controls who can view and speak in the channel.')
        .setRequired(false)
        .addChoices(
          { name: 'Off - Just you and the person doing math', value: 'off' },
          { name: 'Viewing - everyone can see the channel', value: 'viewing' },
          { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
        ))
  )
  .addSubcommand(subcommand =>
    subcommand
      .setName('single_digit_times_table')
      .setDescription('Assign single digit times tables to a user')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to assign times tables to.')
        .setRequired(true))
      .addBooleanOption(option => option.setName('math_test')
        .setDescription('Take a math test! Wrong answers aren\'t immediately revealed.')
        .setRequired(false))
      .addStringOption(option => option.setName('spectator')
        .setDescription('Controls who can view and speak in the channel.')
        .setRequired(false)
        .addChoices(
          { name: 'Off - Just you and the person doing math', value: 'off' },
          { name: 'Viewing - everyone can see the channel', value: 'viewing' },
          { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
        ))
  );


export async function execute(interaction) {
  const subcommand = interaction.options.getSubcommand();
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const spectator = interaction.options.getString('spectator') || 'off';
  const test_mode = interaction.options.getBoolean('math_test');

  if (!await hasConsent(sender, to_user)) {
    await interaction.reply({
      content: 'You do not have consent to set problems for that user.',
      ephemeral: true
    });
    return;
  }

  await interaction.deferReply();

  let reply = '';

  if (test_mode) {
    reply += '# Time for a Math Test!\n';
  }

  reply += `***<@${sender.id}> has given <@${to_user.id}>`;

  if (subcommand === 'problems') {
    const num_problems = interaction.options.getInteger('num_problems');
    const operation = interaction.options.getString('operation') || 'all';
    const hard_mode = interaction.options.getBoolean('hard_mode');

    let finalOperation = operation;
    if (hard_mode) {
      reply += ' hard';
      finalOperation = 'hard_mode';
    }

    reply += ' math problems to solve!***\n';
    reply += await problemUtils.assignProblemsToUser(to_user, sender.id, interaction.guild, num_problems, finalOperation, spectator, test_mode, interaction.channel.id);
  } else if (subcommand === 'single_digit_times_table') {
    reply += ' single digit times table problems to solve!***\n';
    reply += await problemUtils.assignTimesTablesToUser(to_user, sender.id, interaction.guild, spectator, test_mode, interaction.channel.id);
  }

  await interaction.editReply(reply);
}
