import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('roll')
  .setDescription('Rolls a die with a specified side, quantity, and modifier.')
  .addStringOption(option =>
    option.setName('die')
      .setDescription('Choose a die: d2, d4, d6, d8, d10, d12, d20, d100')
      .setRequired(true))
  .addIntegerOption(option =>
    option.setName('quantity')
      .setDescription('Number of dice to roll (default 1)')
      .setRequired(false))
  .addIntegerOption(option =>
    option.setName('modifier')
      .setDescription('Modifier to add to the roll (default 0)')
      .setRequired(false));

export async function execute(interaction) {
  let die = interaction.options.getString('die');
  const quantity = interaction.options.getInteger('quantity') || 1;
  const modifier = interaction.options.getInteger('modifier') || 0;

  // Check if the die input starts with 'd', if not, prepend it
  if (!die.startsWith('d')) {
    die = 'd' + die;
  }

  const sides = parseInt(die.substring(1));
  let result = 0;
  let resultString = '';

  for (let i = 0; i < quantity; i++) {
    const roll = Math.floor(Math.random() * sides) + 1;
    result += roll;
    resultString += roll;
    if (i < quantity - 1) {
      resultString += ' + ';
    }
  }

  if (modifier !== 0) {
    result += modifier;
    resultString += ` + ${modifier}`;
  }

  const dieString = quantity === 1 ? `a ${die}` : `${quantity} ${die}(s)`;
  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setTitle('Dice Roll')
    .setDescription(`You rolled ${dieString} with a modifier of ${modifier}.`)
    .addFields(
      { name: 'Rolls', value: resultString },
      { name: 'Total Result', value: result.toString() }
    );

  await interaction.reply({ embeds: [embed] });
}
