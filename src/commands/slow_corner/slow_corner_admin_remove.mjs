import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import {ensureBrainKeyExists, brain} from '../../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('slow_corner_admin_remove')
  .setDescription('Remove slowdowns from a user.')
  .addUserOption(option =>
    option.setName('sub_user')
      .setDescription('The user the slowdowns were set on')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  // Get user
  let user = interaction.options.getUser('sub_user');

  // Remove slowdowns
  if (ensureBrainKeyExists('slowdown', user.id)) {
    delete brain.data.slowdown[user.id];
    await brain.write();
    await interaction.reply({ content: `All slowdowns added on ${user.username} have been removed.`, ephemeral: true });
  } else {
    await interaction.reply({ content: `${user.username} doesn't have any slowdowns set.`, ephemeral: true });
  }
}
