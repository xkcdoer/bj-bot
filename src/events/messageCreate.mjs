import path from 'path';
import { readdir } from 'fs/promises';


import { fileURLToPath } from 'node:url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const messageCreate = {
  name: 'messageCreate',
  async execute(message) {
    // run multiple things onMessageCreate
    const eventsPath = path.join(__dirname, 'messageCreateListeners');
    const eventFiles = await readdir(eventsPath);
    const jseventFiles = eventFiles.filter(file => file.endsWith('.mjs'));

    for (const file of jseventFiles) {
      const filePath = `file://${path.join(eventsPath, file)}`;
      const messageCreateEvent = await import(filePath);
      const scriptNames = Object.keys(messageCreateEvent);
      for (const scriptName of scriptNames) {
        const script = messageCreateEvent[scriptName];
        script.execute(message);
      }
    }
  },
};

export default messageCreate;
