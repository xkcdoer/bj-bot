// Description:
//   give the corner of shame to a user who asks for it
//
// Commands:
//   hubot shame me
//   hubot shame @user

import { EmbedBuilder } from 'discord.js';
import {
  addShameByUserId,
  clearShameUserRecord,
  createShameRecord,
  defaultCount,
  doesShameCountExist,
  getShameUserRecord,
  shame_timer_time,
  updateShameCount,
} from '../utils/shame.mjs';
import { getRandomMsg } from '../utils/failed_count_msg.mjs';
import { brain } from '../utils/brain.mjs';
import { hasConsent } from '../utils/consent.mjs';
import {backfires} from "../utils/backfire.mjs";

const shame_role = process.env.SHAME_ROLE;
const shame_room = process.env.SHAME_ROOM;
const log_room = process.env.LOGS_ROOM;

const timer_time = shame_timer_time; // 24 hours in milliseconds


export const shame_me_hard = {
  regex: /^shame me hard/i,
  tag_bot: true,
  execute: async function (message) {
    let channel = message.channel;
    let task = await getRandomMsg(message.author, message.guild);
    if (!task) return channel.send("You don't have any count messages set.");
    let embed = new EmbedBuilder()
      .setColor('#0099ff')
      .setTitle('SHAME ON YOU!')
      .addFields({name: "Here's your task.", value: task.text})
    let content = `Heads up <@${message.author.id}>! You've got a special message from <@${task.set_by}>.`;
    channel.send({content: content, embeds: [embed]});

    await createShameRecord(message.author, 0, timer_time, 'shame_me');
    await addShameByUserId(message.guild, message.author.id);
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_me = {
  regex: /^shame me$/i,
  tag_bot: true,
  execute: async function (message) {
    await createShameRecord(message.author, 0, timer_time, 'shame_me');
    await addShameByUserId(message.guild, message.author.id);
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_you = {
  regex: /^shame (on )?you/i,
  tag_bot: true,
  execute: async function (message) {
    message.reply('Nice try. Go to the corner for that!');
    await createShameRecord(message.author, 0, timer_time, 'shame_me');
    await addShameByUserId(message.guild, message.author.id);
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_user = {
  regex: /shame <@/i,
  tag_bot: true,
  execute: async function (message) {
    message.reply('Due to recent improvements and not wanting to maintain ' +
      'them in both places, this command is no more! It has ceased to be. ' +
      'It has gone to meet its maker! It has run down the curtain and joined ' +
      'the bleedin\' choir invisible!! \n\n. . . ahem.\n\n Anyway use `/shame`');
  }
}

export const transfer_shame = {
  regex: /^\?transfer /i,
  tag_bot: false,
  execute: async function (message) {
    let sender = message.author;
    let mentions = message.mentions;
    let to_user = mentions.users.first(); // User object

    if (!to_user) {
      await message.react('❌');
      message.reply('Who?');
    } else if (await backfires(message.member, to_user)) {
      let shame = await getShameUserRecord(sender)
      if (shame) {
        shame.count_to = (shame.count_to ?? defaultCount) * 2;
      } else {
        shame = await createShameRecord(sender, 0, undefined, 'transfer_backfire');
        await addShameByUserId(message.guild, sender.id);
      }

      if (doesShameCountExist(sender)) {
        await updateShameCount(sender, undefined, shame.count_to);
      }

      shame.transfer_backfire = true;
      await brain.write();

      await message.react('💣');
      await message.reply(
          `# Backfire!\n${''
          }${sender} tried to transfer their shame to ${to_user} but it ${''
          }backfired. Their shame has been doubled instead! 😈`
      );
    } else {
      let guild = message.guild;
      // First, does the user have the shame role?
      let role = guild.roles.cache.find(role => role.name === shame_role);
      let guildMember = await guild.members.fetch(sender.id);
      if (!guildMember.roles.cache.some(role => role.name === shame_role)) {
        // User does not have the shame role, fail here
        await message.react('🚫');
        message.reply("You can't transfer a role you don't have.");
      } else {
        // User has shame role, can transfer, maybe

        // You know what? Maybe we should disallow certain types of shame to be transferred
        if (brain.data.count_fail_shame_list[sender.id]) {
          if (brain.data.count_fail_shame_list[sender.id].source === 'cheating_on_lines') {
            message.reply('You know how they say that cheaters never prosper? ' +
              'They never transfer shame either. Cheaters keep their shame to themselves! ' +
              'Wait, or count your way out.');
            return;
          } else if (brain.data.count_fail_shame_list[sender.id].source === 'mass_ping') {
            message.reply('Ha. Fuck you. You don\'t get to transfer ' +
              'shame away when you tried to ping everyone.');
            return;
          } else if (brain.data.count_fail_shame_list[sender.id].transfer_backfire) {
            message.reply(
                'Aww, that\'s cute. That blew up in your face the last time' +
                ' you tried it. Why did you think this time would be better?'
            );
            return;
          }
        }

        // Second, make sure the target isn't already shamed
        let receiver_member = await guild.members.fetch(to_user.id);
        if (receiver_member.roles.cache.some(role => role.name === shame_role)) {
          // The target is already shamed, fail here
          await message.react('🚫');
          message.reply("You can't transfer the role to someone who already has it.");
        } else {
          // The target is not already shamed
          // Third, does the target consent to the user?
          if (!await hasConsent(sender, to_user)) {
            // Target does not consent, stop here
            await message.react('🚫');
            message.reply('You do not have consent to transfer shame to that user.');
          } else {
            // if the person who's giving it away has a shame record
            // base the new record on the old one and delete it, otherwise make a new one
            if (brain.data.count_fail_shame_list[sender.id]) {
              await createShameRecord(
                receiver_member,
                brain.data.count_fail_shame_list[sender.id].number,
                brain.data.count_fail_shame_list[sender.id].timer_ms,
                'transfer',
                brain.data.count_fail_shame_list[sender.id].count_to,
                brain.data.count_fail_shame_list[sender.id].given_by,
                brain.data.count_fail_shame_list[sender.id].channel_id,
                brain.data.count_fail_shame_list[sender.id].one_time_slow,
                brain.data.count_fail_shame_list[sender.id].mistake_penalty
              );
              await clearShameUserRecord(sender.id);
            } else {
              // make a new shame record for person
              await createShameRecord(receiver_member, 0, timer_time, 'transfer');
            }

            await brain.write();

            // Remove role from giving user
            await guildMember.roles.remove(role);
            // Add role to the target
            await receiver_member.roles.add(role);

            const channel = message.guild.channels.cache.get(shame_room);

            // Send in shame room
            channel.send(`<@${guildMember.id}> has passed on their `
                + `Corner of Shame to you <@${receiver_member.id}> `
                + 'as they are either your Dominant or you have agreed to '
                + 'complete their Corner of Shame punishment on their behalf. '
                + 'Type `?punish` to get started, enjoy.');
            message.delete();

            const log_channel = message.guild.channels.cache.get(log_room);
            log_channel.send(`<@${guildMember.id}> has passed on their `
                + `Corner of Shame to <@${receiver_member.id}> `
            );
          }
        }
      }
    }
  }
}
