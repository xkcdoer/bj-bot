// Description:
// embed a task when it's posted


import {wfmUtils} from '../utils/wfm.mjs';
import {EmbedBuilder} from 'discord.js';
import {pingsEveryone} from '../utils/security.mjs';
import {addShameByUserId, createShameRecord} from '../utils/shame.mjs';

/**
 * wfm link
 */
export const wfm_link = {
  regex: /https:\/\/writeforme.org\/task\//i,
  tag_bot: false,
  execute: async function(message) {
    let matches = [...message.content.matchAll(/https:\/\/writeforme.org\/task\/[a-fA-F0-9]+/ig)];
    let urls = matches.map(match => match[0]);

    for (const url of urls) {
      let taskId = url.split('/').pop();
      const details = await wfmUtils.getTaskDetails(taskId);
      let taskDetails = await details.json();

      if (
        pingsEveryone(taskDetails.name) ||
        pingsEveryone(taskDetails.text) ||
        pingsEveryone(url)
      ) {
        await message.reply('Fuck you. And SHAME on you for trying to ping everyone!');
        await addShameByUserId(message.guild, message.author.id);
        await createShameRecord(message.author, 0, 86400000, 'mass_ping');
        return;
      }

      // create an embed message
      const embed = new EmbedBuilder()
        .setColor(0xC83F17)
        .setTimestamp()
        .setFooter({text: 'Task ID: ' + taskDetails._id})
        .setURL(url);

      if (message.channel.nsfw) {
        embed.setTitle(taskDetails.name)
          .setDescription(taskDetails.text)
      } else {
        embed.setTitle('||' + taskDetails.name + '||')
          .setDescription('||' + taskDetails.text + '||')
          .addFields({
            name: 'Why is this spoilered?',
            value: 'Because this isn\'t a NSFW room and there\'s no promise the task is SFW text.'
          });
      }
      // send the embed to the same channel
      message.channel.send({embeds: [embed]});
    }
  }
}
