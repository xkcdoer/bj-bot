/**
 * Message Stacker
 * Use this when looping to build a message so that it can
 *  stack it up and break it apart appropriately.
 */
class MessageStacker {
  constructor(interactionOrMessage, prepend = '', ephemeral = false) {
    this.interactionOrMessage = interactionOrMessage;
    this.prepend = prepend;
    this.ephemeral = ephemeral;
    this.message_cache = '';
    this.isInteraction = this.isInteraction(interactionOrMessage);
    this.isMessage = this.isMessage(interactionOrMessage);
  }

  isInteraction(obj) {
    return obj && typeof obj === 'object' && 'reply' in obj;
  }

  isMessage(obj) {
    return obj && typeof obj === 'object' && 'react' in obj;
  }

  async send(content) {
    if (this.isInteraction) {
      if (this.interactionOrMessage.deferred && !this.interactionOrMessage.replied) {
        await this.interactionOrMessage.editReply({ content: content, ephemeral: this.ephemeral });
      } else if (!this.interactionOrMessage.replied) {
        await this.interactionOrMessage.reply({ content: content, ephemeral: this.ephemeral });
      } else {
        await this.interactionOrMessage.followUp({ content: content, ephemeral: this.ephemeral });
      }
    } else if (this.isMessage) {
      await this.interactionOrMessage.reply(content);
    }
  }

  async react(emoji) {
    if (this.isMessage) {
      await this.interactionOrMessage.react(emoji);
    }
    // else, there's no way to add a reaction to an interaction
  }

  async appendOrSend(message_temp) {
    // Discord character limit of 2000 characters
    if ((this.prepend + this.message_cache + message_temp).length > 2000) {
      await this.send(this.prepend + this.message_cache);
      this.message_cache = message_temp;
      this.prepend = '';
    } else {
      this.message_cache += message_temp;
    }
  }

  async finalize(append = '', no_message = '') {
    if ((this.prepend + this.message_cache).length === 0
      && no_message.length > 0
    ) {
      await this.send(no_message);
    } else if ((this.prepend + this.message_cache).length === 0) {
      await this.react('🤷‍♀️');
    } else {
      if ((this.prepend + this.message_cache + append).length > 2000) {
        await this.send(this.prepend);
        await this.send(this.message_cache);
        await this.send(append);
      } else {
        await this.send(this.prepend + this.message_cache + append);
      }
    }
  }
}

export { MessageStacker };
