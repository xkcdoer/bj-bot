

export function pingsEveryone(messageContent) {
  if (typeof messageContent === 'string' && messageContent) {
    return messageContent.match(/@everyone/i) || messageContent.match(/@here/i);
  } else {
    return false;
  }
}
