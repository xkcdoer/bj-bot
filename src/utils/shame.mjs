import {brain, ensureBrainKeyExistsPromise} from './brain.mjs';
import { addRoleByName, getGuildMemberByUserID } from './users.mjs';

const shame_role = process.env.SHAME_ROLE;
export const defaultCount = 100;
export const shame_timer_time = 86400000; // 24 hours in milliseconds


// RETURNS A PROMISE
// Because it has to look up the guild member
export const userHasShameByID = async function(guild, user_id) {
  let guildMember = await getGuildMemberByUserID(guild, user_id);
  // return if the user has the shame role or not
  return guildMember.roles.cache.some(role => role.name === shame_role);
};

export async function clearShameUserRecord(user) {
  let user_id;

  if (user.id) {
    user_id = user.id;
  } else {
    user_id = user;
  }
  await ensureBrainKeyExistsPromise('count_fail_shame_list');

  delete brain.data.count_fail_shame_list[user_id];
  await brain.write();
}

export const createShameRecord = async function(user, number, timer, source, count_to = null, given_by_user_id = null, channel_id = null, oneTimeSlowdown = null, mistakePenalty = null) {
  let time_now = new Date().getTime();
  let over;
  if (timer) {
    over = time_now + timer;
  } else {
    // 24 hours in milliseconds
    over = time_now + 86400000;
  }
  let user_id;
  if (user.id) {
    user_id = user.id;
  } else {
    user_id = user;
  }

  await ensureBrainKeyExistsPromise('count_fail_shame_list');
  const shame = brain.data.count_fail_shame_list[user_id] = {
    'user_id': user_id,
    'number': number,
    'given_by': given_by_user_id,
    'channel_id': channel_id,
    'received': time_now,
    'timer_ms': timer,
    'over': over,
    'source': source,
    'count_to': count_to,
    'one_time_slow': oneTimeSlowdown,
    'mistake_penalty': mistakePenalty
  };

  await brain.write();
  return shame;
}

export const getShameUserRecord = async function(user) {
  let user_id;

  if (user.id) {
    user_id = user.id;
  } else {
    user_id = user;
  }

  let shame_list = await ensureBrainKeyExistsPromise('count_fail_shame_list');

  if (shame_list[user_id]) {
    return shame_list[user_id];
  }
};

export const shameUpdate = async function(user_id, timer) {
  let time_now = new Date().getTime();
  let over = time_now + timer;
  await ensureBrainKeyExistsPromise('count_fail_shame_list', user_id);
  brain.data.count_fail_shame_list[user_id].over = over;
  brain.data.count_fail_shame_list[user_id].timer_ms = timer;
  await brain.write();
}

export const addToShameCountTarget = async function(user, additional) {
  // Ensure the existence of the key `count_fail_shame_list`
  await ensureBrainKeyExistsPromise('count_fail_shame_list');

  // Ensure the user has an existing shame record
  if (!brain.data.count_fail_shame_list[user.id]) {
    // there is none? Set one up
    await createShameRecord(user, null, null, 'added_to_nothing', defaultCount);
  }

  // Add the additional count to the current count_to value
  brain.data.count_fail_shame_list[user.id].count_to += additional;

  // Write the changes to the brain
  await brain.write();
};

// Why ask for user ID and not user? Because this is more universal
// Roles require a GUILD MEMBER not USER,
// so if I ask for user_id I can always ensure I'm using the right object
export const addShameByUserId = async function(guild, user_id) {
  await addRoleByName(user_id, guild, shame_role);
}



////////////////////////////////////////////////
//       Functions for handling ?punish       //
////////////////////////////////////////////////

export const setupShameCount = async function(user, target = defaultCount, using_last_fail = false) {
  // force the 'shame_counts' key to exist if needed
  await ensureBrainKeyExistsPromise('shame_counts');
  // User's count and target count
  brain.data.shame_counts[user.id] = {
    count: 0,
    target: target,
    using_last_fail: using_last_fail,
    stopped: true,
    violations: 0,
  };
  await brain.write;
};

export function doesShameCountExist(user) {
  return !!(brain.data.shame_counts && brain.data.shame_counts[user.id]);
}

export const updateShameCount = async function(user, count, target) {
  if (!brain.data.shame_counts || !brain.data.shame_counts[user.id]) {
    throw new Error(`No existing shame_count record for user ${user.id}`);
  }
  if (count !== undefined) {
    brain.data.shame_counts[user.id].count = count;
  }
  if (target !== undefined) {
    brain.data.shame_counts[user.id].target = target;
  }
  await brain.write();
  return target;
};

export async function addToTarget(user, additional) {
  let target = brain.data.shame_counts[user.id].target;
  await addToShameCountTarget(user, additional);
  return await updateShameCount(user, undefined, target + +additional);
}

export function calculatePunishment(failed_number) {
  if (failed_number < 100) {
    return 100;
  } else if (failed_number <= 450) {
    return 100 + Math.ceil(failed_number / 3);
  } else {
    return 250;
  }
}
